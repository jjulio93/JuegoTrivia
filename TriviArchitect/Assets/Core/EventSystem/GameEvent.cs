﻿using UnityEngine;
using System.Collections;

public class GameEvent {

	private GameObject target;

	public GameEvent(GameObject target){
		this.target = target;
	}

	public GameObject getTarget(){
		return target;
	}

}
