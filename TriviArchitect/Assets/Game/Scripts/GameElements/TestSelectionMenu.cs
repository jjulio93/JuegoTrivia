﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSelectionMenu : MonoBehaviour {

	public GameObject buttonPrefab;

	private object[] aviableTests;
	private GameObject buttonPanel;

	void Awake(){
		buttonPanel = GameObject.Find ("TestPanel");
	}

	void Start(){
		aviableTests = Resources.LoadAll ("ScriptableObjects/Tests", typeof(Test));
		initializeMenu ();
	}

	private void initializeMenu(){
		for (int i = 0; i < aviableTests.Length; i++) {
			GameObject instance = (GameObject)Instantiate (buttonPrefab,buttonPanel.transform);
			instance.transform.localScale = Vector3.one;
			instance.GetComponent<TestSelectionButton> ().init ((Test)aviableTests[i]);
		}
	}
		

}
