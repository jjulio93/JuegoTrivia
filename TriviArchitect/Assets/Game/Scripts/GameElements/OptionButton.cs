﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class OptionButton : MonoBehaviour, IPointerClickHandler {

	private int answer = 0;

	public void init(int answer, string text){
		this.answer = answer;
		GetComponentInChildren<Text> ().text = text;
		gameObject.SetActive (true);
	}

	public void deactivate(){
		gameObject.SetActive (false);
	}

	#region IPointerClickHandler implementation

	public void OnPointerClick (PointerEventData eventData)
	{
		TestManager.instance.submitAnswer (answer);
	}

	#endregion


}
