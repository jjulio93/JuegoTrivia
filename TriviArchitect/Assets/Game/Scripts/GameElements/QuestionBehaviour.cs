﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionBehaviour : MonoBehaviour {

	public GameObject optionPrefab;
	private Question data;

	private Transform buttonPanel;
	private Text questionText;

	void Awake(){
		buttonPanel = transform.GetChild (1);
		questionText = transform.GetChild (0).GetChild (0).GetComponent<Text> ();
	}

	public void setData(Question data){
		this.data = data;
	}

	public void init(){
		questionText.text = data.questionText;
		makeButtons ();
		gameObject.SetActive (true);
	}

	public void deactivate(){
		for (int i = 0; i < buttonPanel.childCount; i++) {
			buttonPanel.GetChild (i).GetComponent<OptionButton> ().deactivate ();
		}
		gameObject.SetActive (false);
	}

	public Question getData(){
		return data;
	}

	private void makeButtons(){
		for (int i = 0; i < data.options.Length; i++) {
			GameObject button;
			if (i < transform.GetChild (1).childCount) {
				button = buttonPanel.GetChild (i).gameObject;
			} else {
				button = (GameObject)Instantiate (optionPrefab, transform.position, Quaternion.identity);
				button.transform.SetParent (buttonPanel, true);
				button.transform.localScale = Vector3.one;
			}
			button.GetComponent<OptionButton> ().init (i, data.options[i]);
		}
	}

}
