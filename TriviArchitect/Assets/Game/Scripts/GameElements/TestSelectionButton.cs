﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TestSelectionButton : MonoBehaviour, IPointerClickHandler {

	private Test test;
	private Text buttonText;

	void Awake(){
		buttonText = GetComponentInChildren<Text> ();
	}

	public void init(Test test){
		this.test = test;
		buttonText.text = test.name; 
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		AppManager.instance.startGame (test);
	}
	#endregion
}
