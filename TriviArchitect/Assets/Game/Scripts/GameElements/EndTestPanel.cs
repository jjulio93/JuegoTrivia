﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EndTestPanel : MonoBehaviour, IPointerClickHandler{

	private const string NEXT_SCENE = "TestSelection";

	private Text message;

	void Awake(){
		message = transform.GetChild(0).GetComponentInChildren<Text> ();
	}

	public void init(string text){
		this.message.text = text;
		transform.GetChild(0).gameObject.SetActive (true);
	}

	public void deactivate(){
		transform.GetChild(0).gameObject.SetActive (false);
	}

	#region IPointerClickHandler implementation
	public void OnPointerClick (PointerEventData eventData)
	{
		deactivate ();
		SceneManager.instance.loadScene (NEXT_SCENE);
	}
	#endregion
}
