﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoadedEvent : GameEvent {

	string loadedScene;

	public SceneLoadedEvent(GameObject target, string loadedScene) : base(target){
		this.loadedScene = loadedScene;
	}

	public string getLoadedScene(){
		return loadedScene;
	}

}
