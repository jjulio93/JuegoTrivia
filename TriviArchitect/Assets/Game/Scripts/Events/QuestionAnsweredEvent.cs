﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionAnsweredEvent : GameEvent {
	private bool correct;

	public QuestionAnsweredEvent(GameObject target, bool correct) : base(target){
		this.correct = correct;
	}

	public bool isCorrect(){
		return correct;
	}

}
