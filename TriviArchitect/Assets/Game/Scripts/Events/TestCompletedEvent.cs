﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCompletedEvent : GameEvent {

	private float score;
	private float completionTime;

	public TestCompletedEvent(GameObject target, float score, float completionTime) : base(target){
		this.score = score;
		this.completionTime = completionTime;
	}

	public float getScore(){
		return score;
	}

	public float getCompletionTime(){
		return completionTime;
	}

}
