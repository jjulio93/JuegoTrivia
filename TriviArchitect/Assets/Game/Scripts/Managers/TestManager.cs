﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestManager : MonoBehaviour {

	private static TestManager self;
	public static TestManager instance{
		get{
			if (self == null) {
				self = GameObject.FindObjectOfType <TestManager> ();
			}
			return self;
		}
	}

	private Test testData;

	private int score;
	private int currentQuestion;
	private float timer;

	private Text timerText;
	private QuestionBehaviour questionObject;

	private bool testInProgress;

	void Awake(){
		if (TestManager.self != null) {
			Destroy (gameObject);
		}
		questionObject = GameObject.FindObjectOfType<QuestionBehaviour> ();
		timerText = GameObject.Find ("Timer").GetComponent<Text> ();
	}

	void Update(){
		if (testInProgress && !PauseManager.instance.isGamePaused()) {
			timer += Time.deltaTime;
			timerText.text = Mathf.Floor (timer).ToString();
		}
	}

	public void initTest(Test testData){
		score = 0;
		currentQuestion = 0;
		this.testData = testData;
		testInProgress = true;
		setQuestion (currentQuestion);
	}

	public void submitAnswer(int answer){
		if (this.testData.questions [currentQuestion].answer == answer) {
			score++;
		}
		setQuestion (++currentQuestion);
	}

	private void setQuestion(int currentQuestion){
		if (currentQuestion >= testData.questions.Length) {
			endTest ();
		}
		else{
			questionObject.deactivate ();
			questionObject.setData (this.testData.questions [currentQuestion]);
			questionObject.init ();
		}
	}


	private void endTest(){
		testInProgress = false;
		EventManager.instance.InvokeEvent<TestCompletedEvent> (new TestCompletedEvent (gameObject, (float)score/(float)testData.questions.Length, timer));
	}

}
