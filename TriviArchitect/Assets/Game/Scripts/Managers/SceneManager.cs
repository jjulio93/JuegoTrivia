﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour {

	private const float MIN_SCENE_LOADING_TIME = 0.5f;

	private static SceneManager self;
	public static SceneManager instance{
		get{
			if (self == null) {
				self = GameObject.FindObjectOfType<SceneManager> ();
			}
			return self;
		}
	}

	void Awake(){
		if (self != null) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
	}

	public void loadScene(string sceneName){
		StartCoroutine (loadProcess (sceneName));	
	}

	private IEnumerator loadProcess(string sceneName){
		AsyncOperation op = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync (sceneName);
		op.allowSceneActivation = false;
		while (op.progress < 0.9f) {
			Debug.Log (op.progress);
			yield return 0;
		}
		yield return new WaitForSeconds (MIN_SCENE_LOADING_TIME);

		op.allowSceneActivation = true;
		yield return 0;
		EventManager.instance.InvokeEvent<SceneLoadedEvent> (new SceneLoadedEvent (gameObject, sceneName));
	}

}
