﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour {

	private const string GAMEPLAY_SCENE_NAME = "Gameplay";

	private static AppManager self;
	public static AppManager instance{ 
		get{
			if (self == null) {
				self = GameObject.FindObjectOfType<AppManager> ();
			}
			return self;
		}
	}

	private Test currentTest;

	void Awake(){
		if (self != null) {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (gameObject);
		EventManager.instance.AddListener<SceneLoadedEvent> (onSceneLoaded);
	}

	void Start(){
		initApplication ();
	}

	public void startGame(Test test){
		currentTest = test;
		SceneManager.instance.loadScene (GAMEPLAY_SCENE_NAME);
	}

	public void  quitApplication(){
		Application.Quit ();
	}

	private void initApplication(){
		Debug.Log ("Aplication Initialized Successfully");
	}

	public void onSceneLoaded(SceneLoadedEvent eventData){
		if (eventData.getLoadedScene ().Equals (GAMEPLAY_SCENE_NAME)) {
			GameplayManager.instance.initGame (currentTest);
		}
	}

}
