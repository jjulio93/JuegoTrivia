﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour {
	private static GameplayManager self;
	public static GameplayManager instance {
		get{
			if (self == null) {
				self = GameObject.FindObjectOfType<GameplayManager> ();
			}
			return self;
		}
	}

	public Test test;
	private  EndTestPanel endTestPanel;

	void Awake(){
		if (self != null) {
			Destroy (gameObject);
		}

		EventManager.instance.AddListener<TestCompletedEvent> (OnTestCompleted);
		endTestPanel = GameObject.FindObjectOfType<EndTestPanel> ();
	}

	public void quitGame(){
		SceneManager.instance.loadScene ("TestSelection");
	}

	public void initGame(Test test){
		TestManager.instance.initTest (test);
	}

	public void OnTestCompleted(TestCompletedEvent eventData){
		float percentScore = eventData.getScore () * 100;
		string message = "Prueba completada \n Tu puntaje: " + ((int)percentScore).ToString() + "%";

		endTestPanel.init (message);
	}
		
}
