﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour {

	private static PauseManager self;
	public static PauseManager instance {
		get{
			if (self == null) {
				self = GameObject.FindObjectOfType<PauseManager> ();
			}
			return self;
		}
	}

	private GameObject pausePanel;
	private bool  gamePaused = false;

	void Awake(){
		if (self != null) {
			Destroy (gameObject);
		}
		pausePanel = GameObject.Find ("PausePanel");
	}

	public bool isGamePaused(){
		return gamePaused;
	}

	public void pauseGame(){
		pausePanel.transform.GetChild (0).gameObject.SetActive (true);
		gamePaused = true;
	}

	public void unPauseGame(){
		pausePanel.transform.GetChild (0).gameObject.SetActive (false);
		gamePaused = false;
	}

}
