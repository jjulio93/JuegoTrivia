﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName= "Entities/Test")]
public class Test : ScriptableObject {
	public int id;
	public string testName;
	public Question[] questions;
	public float time;
}
