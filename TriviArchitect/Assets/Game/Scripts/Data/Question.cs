﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Entities/Question")]
public class Question : ScriptableObject {
	public int id;
	public string level;
	public string questionText;
	public string[] options;
	public int answer;
}	
