﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ImportQuestions : Editor{

	private const string QUESTION_FOLDER_PATH = "Assets/Resources/ScriptableObjects/Questions";
	private static string QUESTION_FILE_METANAME = "/New" + typeof(Question).ToString () + ".asset";

	[MenuItem("Tools/ Importar Preguntas")]
	public static void importQuestions(){
		string data = readFile ();
		if (data != "") {
			string[,] processedData = processCSV (data);
			DebugOutputGrid (processedData);
			createScriptableObjects (processedData);
		}
	}

	static public void DebugOutputGrid(string[,] grid)
	{
		string textOutput = ""; 
		for (int y = 0; y < grid.GetUpperBound(1); y++) {	
			for (int x = 0; x < grid.GetUpperBound(0); x++) {
				textOutput += grid[x,y]; 
				textOutput += "|"; 
			}
			textOutput += "\n"; 
		}
		Debug.Log(textOutput);
	}

	private static string[,] processCSV(string csvText){
		string[] lines = csvText.Split("\n"[0]); 

		// finds the max width of row
		int width = 0; 
		for (int i = 0; i < lines.Length; i++)
		{
			string[] row = lines[i].Split(','); 
			width = Mathf.Max(width, row.Length); 
		}

		// creates new 2D string grid to output to
		string[,] outputGrid = new string[width + 1, lines.Length + 1]; 
		for (int y = 0; y < lines.Length; y++)
		{
			string[] row = lines [y].Split (',');
			for (int x = 0; x < row.Length; x++) 
			{
				outputGrid[x,y] = row[x]; 
				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}

		return outputGrid; 
	}

	private static void createScriptableObjects(string[,] data){
		Debug.Log (data.GetLength (1));
		for (int i = 1; i < data.GetLength(1); i++) {
			if (data [0, i] != "") {
				
				object[] values = getData (data, i);
				Question instance = makeQuestion (values);

				string assetPathName = AssetDatabase.GenerateUniqueAssetPath (QUESTION_FOLDER_PATH + QUESTION_FILE_METANAME);
				AssetDatabase.CreateAsset (instance, assetPathName);
			}
			AssetDatabase.SaveAssets ();
			AssetDatabase.Refresh ();
			EditorUtility.FocusProjectWindow ();
		}
	}


	private static string readFile(){
		string path = EditorUtility.OpenFilePanel ("Seleccionar un archivo", "", "csv");
		if (path != "") {
			Debug.Log (path);
			return File.ReadAllText (path);
		} else {
			return "";
		}
	}

	private static object[] getData(string[,] data, int i){
		string level = data [0, i];
		string questionText = data [1, i];
		int amount = Random.Range (3, 5);

		string[] answers = new string[amount];
		int correctAnswer = Random.Range (0, amount);
		answers [correctAnswer] = data [2, i];

		answers = generateOptions (data, answers, correctAnswer);

		object[] values = { level, questionText, answers, correctAnswer };
		return values;
	}

	private static Question makeQuestion(object[] values){
		Question instance = ScriptableObject.CreateInstance<Question> ();

		instance.answer = (int)values[3];
		instance.questionText = (string)values[1];
		instance.level = (string)values[0];
		instance.options = (string[])values[2];
		return instance;
	}

	private static string[] generateOptions(string[,] data, string[] answers, int correctAnswer){
		for (int x = 0; x < answers.Length; x++) {
			if (x != correctAnswer) {
				int row = 0;
				do {
					row = Random.Range(0, data.GetLength(1));
				} while(data [2, row] == "" || data[2,row] == " ");
				answers [x] = data [2, row];
			}
		}
		return answers;
	}

}
